import React from "react";
import NavBar from "../components/navBar";
import MenuHeader from "../components/menuHeader";

const homeScreen = () => {
  return (
    <div>
      <MenuHeader />
      <NavBar />
      <h1>Home</h1>
    </div>
  );
};

export default homeScreen;
