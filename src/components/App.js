import React from "react";
import { BrowserRouter, Route } from "react-router-dom";

import HomeScreen from "../screens/homeScreen";
import orderScreen from "../screens/orderScreen";
import productScreen from "../screens/productScreen";
import statScreen from "../screens/statScreen";

const App = () => {
  return (
    <BrowserRouter>
      <div>
        <Route path="/" exact component={HomeScreen} />
        <Route path="/orders" exact component={orderScreen} />
        <Route path="/products" exact component={productScreen} />
        <Route path="/stats" exact component={statScreen} />
      </div>
    </BrowserRouter>
  );
};
export default App;
