import React from "react";
import { Link } from "react-router-dom";
const menuHeader = props => (
  <header style={{ marginTop: 70 }}>
    <h1 class="display-3 text-center my-4">Welcome User</h1>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="dropdown">
            <Link to="/" class="btn btn-primary btn-block" type="button">
              Inicio
            </Link>
          </div>
        </div>
        <div class="col-md-3">
          <div class="dropdown">
            <Link to="/orders" class="btn btn-success btn-block" type="button">
              Pedidos
            </Link>
          </div>
        </div>
        <div class="col-md-3">
          <div class="dropdown">
            <button class="btn btn-warning btn-block" type="button">
              Productos
            </button>
          </div>
        </div>
        <div class="col-md-3">
          <div class="dropdown">
            <button
              class="btn btn-danger btn-block"
              type="button"
              data-toggle="dropdown"
            >
              Estadísticas
            </button>
          </div>
        </div>
      </div>
    </div>
    <hr />
  </header>
);

export default menuHeader;
