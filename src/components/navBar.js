import React from "react";
const navBar = props => (
  <nav class="navbar fixed-top navbar-expand-sm navbar-light bg-light mb-3">
    <div class="container">
      <a class="navbar-brand" href="#">
        Tencargo
      </a>
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" href="#welcome">
            About
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#about">
            Services
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#services">
            User
          </a>
        </li>
      </ul>
    </div>
  </nav>
);
export default navBar;
